export enum ELogModule {
  Config = 'Config',
  Balancer = 'Balancer',
  RecManager = 'RecManager',
  RecDump = 'RecDump',
  RecFs = 'RecFs',
  RecJournal = 'RecJournal',
}
