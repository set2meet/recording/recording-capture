# syntax=docker/dockerfile:experimental

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

###########################################
# builder
###########################################
FROM node:10.20-alpine3.9 AS builder

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN apk add \
    build-base \
    linux-headers \
    python \
    pkgconfig \
    openssh \
    git

WORKDIR /usr/recording-capture
# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

COPY ./ ./

RUN npm set unsafe-perm true
RUN npm install --global rimraf cross-env typescript
RUN --mount=type=ssh,id=default npm install && \
    npm run test && \
    npm run lint && \
    npm run build
RUN --mount=type=ssh,id=default [[ -f config/default.json ]] || npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git' -- -module recording-capture -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}


###########################################
# recording-capture
###########################################
FROM node:10.20-alpine3.9

RUN npm i -g forever

WORKDIR /usr/set2meet
# Copy config
COPY --from=builder /usr/recording-capture/config ./config/
# Copy build
COPY --from=builder /usr/recording-capture ./recording-capture

EXPOSE 3350

ENTRYPOINT ["forever", "recording-capture/dist/index.js"]
