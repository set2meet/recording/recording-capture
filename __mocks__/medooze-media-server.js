﻿'use strict';

const MediaServer = {
  recorder: null,

  terminate: () => {},
  enableLog: () => {},
  enableDebug: () => {},
  setPortRange: () => {},
  enableUltraDebug: () => {},
  createEndpoint: () => {},
  createRecorder: () => { return MediaServer.recorder; },
  createPlayer: () => {},
  createStreamer: () => {},
  createActiveSpeakerDetector: () => {},
  createRefresher: () => {},
  createEmulatedTransport: () => {},
  getDefaultCapabilities: () => {},
};

module.exports = MediaServer;
