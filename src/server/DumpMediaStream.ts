import logger from '../logger/logger';
import { SDPInfo, StreamInfo, SupportedMedia } from 'semantic-sdp';
import MediaServer, { Recorder, Transport, IncomingStreamTrack, Endpoint } from 'medooze-media-server';

type TTrackKind = 'audio' | 'video';

const outgoingStreamInfo = {
  audio: false,
  video: false,
};

type TCapabilities = {
  [k: string]: SupportedMedia;
};

const capabilities: TCapabilities = {
  audio: {
    codecs: ['opus'],
    rtcpfbs: [],
    extensions: [
      'urn:ietf:params:rtp-hdrext:ssrc-audio-level',
      'http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01',
    ],
  },
  video: {
    codecs: ['vp9', 'h264;packetization-mode=1'],
    rtx: true,
    rtcpfbs: [{ id: 'transport-cc' }, { id: 'ccm', params: ['fir'] }, { id: 'nack' }, { id: 'nack', params: ['pli'] }],
    extensions: ['http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01'],
  },
};

export type TDumpMediaStreamOptions = {
  output: string;
  description: RTCSessionDescription;
  endpoint: Endpoint;
};

export type TAnswer = {
  [key: string]: string;
};

export default class DumpMediaStream {
  private output: string;
  private answerSDP: SDPInfo;
  private recorder: Recorder;
  private transport: Transport;
  private logger = logger.recDump;
  private endpoint: Endpoint;

  constructor(options: TDumpMediaStreamOptions) {
    this.endpoint = options.endpoint;

    if (options.description) {
      this.output = options.output;
      this.initDump(options.description);
    } else {
      throw new Error('no-offer-description');
    }
  }

  public get answer(): TAnswer {
    return {
      type: 'answer',
      sdp: this.answerSDP.toString(),
    };
  }

  public stop(): void {
    this.answerSDP = null;
    this.output = null;

    if (this.recorder) {
      this.recorder.stop();
      this.recorder = null;
    }

    if (this.transport) {
      this.transport.stop();
      this.transport = null;
    }

    this.logger.info('stop');
  }

  private initDump(description: RTCSessionDescription): void {
    this.logger.info('init', {
      description,
    });

    // parse sdp description
    const offer = SDPInfo.parse(description.sdp);

    // create media recorder
    this.recorder = MediaServer.createRecorder(this.output, {
      waitForIntra: true,
    });

    // create an DTLS ICE transport in that enpoint
    this.transport = this.endpoint.createTransport(offer);

    // debug log
    this.transport.on('incomingtrack', (track) => {
      this.logger.info('incomingtrack', {
        id: track.getId(),
        info: track.getTrackInfo(),
      });
    });

    // set RTP remote properties
    this.transport.setRemoteProperties(offer);

    // create local SDP info
    const answer = offer.answer({
      dtls: this.transport.getLocalDTLSInfo(),
      ice: this.transport.getLocalICEInfo(),
      candidates: this.endpoint.getLocalCandidates(),
      capabilities: capabilities,
    });

    // set RTP local  properties
    this.transport.setLocalProperties(answer);

    // record each stream offered
    for (const offered of offer.getStreams().values()) {
      this.recordStream(answer, offered);
    }

    // save answer
    this.answerSDP = answer;
  }

  private recordStream(answer: SDPInfo, offered: StreamInfo): void {
    // create the remote stream into the transport
    const stream = this.transport.createIncomingStream(offered);

    // create new local stream with only audio
    const outgoingStream = this.transport.createOutgoingStream(outgoingStreamInfo);

    // get local stream info
    const info = outgoingStream.getStreamInfo();

    // copy incoming data from the remote stream to the local one
    outgoingStream.attachTo(stream);

    // add local stream info it to the answer
    answer.addStream(info);

    const at = stream.getAudioTracks()[0];
    const vt = stream.getVideoTracks()[0];

    this.logger.info('media', {
      audio: at?.getId(),
      video: vt?.getId(),
    });

    if (at) {
      this.logMediaStatusUpdate(at, 'audio');
    }
    if (vt) {
      this.logMediaStatusUpdate(vt, 'video');
    }

    // record it
    this.recorder.record(stream);
  }

  private logMediaStatusUpdate(track: IncomingStreamTrack, kind: TTrackKind): void {
    track.on('attached', () => this.logTrackEvent('attached', track, kind));
    track.on('detached', () => this.logTrackEvent('detached', track, kind));
    track.on('stopped', () => this.logTrackEvent('stopped', track, kind));
  }

  private logTrackEvent(event: string, track: IncomingStreamTrack, kind: TTrackKind): void {
    this.logger.info(event, {
      kind,
      id: track.getId(),
    });
  }
}
