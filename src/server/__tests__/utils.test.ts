jest.mock('fs-extra');
jest.mock('@s2m/cloud');

import path from 'path';
import fs from 'fs-extra';

import { checkDirExistanceOrCreate, getNextRecordFileName, clearEmptyDirectories } from '../utils';

describe('utils', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  test('checkDirExistanceOrCreate', () => {
    const folder = 'a folder';

    checkDirExistanceOrCreate(folder);

    expect(fs.ensureDirSync).toBeCalledWith(folder);
  });

  test('getNextRecordFileName without files', () => {
    const folder = 'a folder';

    (fs.readdirSync as jest.Mock).mockReturnValue([]);

    const nextFile = getNextRecordFileName(folder);

    expect(nextFile).toEqual('part-1.mp4');
  });

  test('getNextRecordFileName with several files', () => {
    const folder = 'a folder';

    (fs.readdirSync as jest.Mock).mockReturnValue(['part-1.mp4', 'part-2.mp4', 'part-3.mp4']);

    const nextFile = getNextRecordFileName(folder);

    expect(nextFile).toEqual('part-4.mp4');
  });

  test('clearEmptyDirectories', () => {
    const root = 'root';
    const subfolder1 = 'subfolder1';
    const subfolder2 = 'subfolder2';
    const fullsubfolder1 = path.resolve(root, subfolder1);
    const fullsubfolder2 = path.resolve(root, subfolder2);

    (fs.readdirSync as jest.Mock).mockImplementation((dir) => {
      if (dir === root) {
        return [subfolder1, subfolder2];
      }

      if (dir === fullsubfolder1) {
        return ['file1', 'file2'];
      }

      return [];
    });

    clearEmptyDirectories(root);

    expect(fs.rmdirSync).toBeCalledWith(fullsubfolder2);
  });
});
