/* istanbul ignore file */
export default {
  invalidAuthentication: {
    code: 1,
    message: 'Authentication error',
  },

  maxConnectionsExceeded: {
    code: 2,
    message: 'Max connections exceeded',
  },
};
