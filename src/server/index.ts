/* istanbul ignore file */
import minimist from 'minimist';
import get from 'lodash/get';
import * as https from 'https';
import * as http from 'http';
import SocketIO from 'socket.io';
import errors from './errors';
import { buildConfig } from './config';
import ConnectionBalancer from './ConnectionBalancer';
import RecordingCaptureService from './RecordingCaptureService';
import MediaServer, { Endpoint } from 'medooze-media-server';
import Koa, { Context } from 'koa';
import Router from 'koa-router';
import logger from '../logger/logger';
import { ELogErrorCode } from '../logger/ELogErrorCode';
import { ServerOptions } from 'https';

type TServerArguments = {
  udpPortMin: number;
  udpPortMax: number;
  externalIP: string;
  port: number;
};

const getServerArguments = (): TServerArguments => {
  // eslint-disable-next-line no-magic-numbers
  const argv = minimist(process.argv.slice(2));
  //UDP port range for endpoints
  const value = get(argv, 'endpoints-port-range', '50000-60000') as string;
  const [minPortValue, maxPortValue] = value.split(/-/);
  const rest: string[] = argv._;
  //External IP address of server, to be used when announcing the local ICE candidate
  const firstArgumentIndex = 0;
  const externalIP: string = rest[firstArgumentIndex] || '127.0.0.1';
  const DEFAULT_PORT = 3350;
  const port: number = parseInt(get(argv, 'port', DEFAULT_PORT));

  return {
    udpPortMin: parseInt(minPortValue),
    udpPortMax: parseInt(maxPortValue),
    externalIP,
    port,
  };
};

const args = getServerArguments();

MediaServer.setPortRange(args.udpPortMin, args.udpPortMax);
//createEndpoint must be after MediaServer.setPortRange
const endpoint: Endpoint = MediaServer.createEndpoint(args.externalIP);

const socketOptions = {
  origins: '*:*',
  path: '/ws',
  pingTimeout: 3000,
  pingInterval: 3000,
};

const createHttpServer = (serverOptions: ServerOptions): http.Server => {
  const app = new Koa<Context>();
  const router = new Router();

  router.get('/health', (ctx: Context) => (ctx.body = 'ok'));

  app.use(router.routes());

  return process.env.NODE_ENV === 'development'
    ? // eslint-disable-next-line @typescript-eslint/no-misused-promises
      https.createServer(serverOptions, app.callback())
    : // eslint-disable-next-line @typescript-eslint/no-misused-promises
      http.createServer(app.callback());
};

const createSocketServer = async (): Promise<void> => {
  const config = await buildConfig();
  const balancer = new ConnectionBalancer(config.maxConnections);
  const httpServer = createHttpServer(config.httpsOptions);
  const socketIO = SocketIO(httpServer, socketOptions);

  // restrict max number of connections
  socketIO.use((socket, next) => {
    if (balancer.blocked) {
      next(errors.maxConnectionsExceeded);
      logger.app.error(
        ELogErrorCode.Application, 
        'New connection blocked', 
        { 
          error: errors.maxConnectionsExceeded 
        }
      );
    } else {
      next();
      balancer.add();
      logger.app.info('New connection');
      socket.on('disconnect', () => balancer.del());
    }
  });

  // todo: check authorization!

  // run server
  httpServer.listen(args.port, () => {
    socketIO.on('connect', (socket) => {
      new RecordingCaptureService(config, socket, endpoint);
    });
  });

  logger.app.info('Service start');
};

createSocketServer();
