import { ServerLogger, S2MServices } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';
import { ELogModule } from './ELogModule';

const appLogger: ServerLogger<ELogModule, ELogErrorCode> = new ServerLogger(
  S2MServices.RecordingCapture
);

const configLogger = appLogger.createBoundChild(ELogModule.Config);
const balancerLogger = appLogger.createBoundChild(ELogModule.Balancer);
const recManagerLogger = appLogger.createBoundChild(ELogModule.RecManager);
const recDumpLogger = appLogger.createBoundChild(ELogModule.RecDump);
const recFsLogger = appLogger.createBoundChild(ELogModule.RecFs);
const recJournalLogger = appLogger.createBoundChild(ELogModule.RecJournal);

export default {
  app: appLogger,
  config: configLogger,
  balancer: balancerLogger,
  recManager: recManagerLogger,
  recDump: recDumpLogger,
  recFs: recFsLogger,
  recJournal: recJournalLogger,
};
