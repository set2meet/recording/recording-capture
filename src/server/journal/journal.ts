/* istanbul ignore file */
import fs from 'fs-extra';
import Journal from '@s2m/journal';
import { getQueue, getStorage, TaskActionType } from '@s2m/cloud';
import { TRecordingConfig } from 'app';
import { FinalStep } from './FinalStep';
import logger from '../../logger/logger';
import { ELogErrorCode } from '../../logger/ELogErrorCode';

export type TFinalDescDump = {
  id: string; // record id
  src: string; // workind directory path
  type: string; //container type
};

export type TFinalDescTask = {
  id: string; // record id
  src: string; // s3 bucket folder key
  type: string; //container type
};

export type TFinalActionError = {
  step: FinalStep;
  rec: TFinalDescDump | TFinalDescTask;
};

const saveDumpToCloud = async (rec: TFinalDescDump): Promise<string> => {
  logger.recManager.info('upload start', {
    id: rec.id,
  });

  try {
    const s3key = await getStorage().uploadRecordingCaptureVideos(rec.id, rec.src);

    fs.removeSync(rec.src);

    logger.recManager.info('upload complete', {
      id: rec.id,
      dst: s3key,
    });

    return s3key;
  } catch (err) {
    const error = err as Error;

    throw { rec, step: FinalStep.dump, error };
  }
};

const createTaskProcess = async (rec: TFinalDescTask): Promise<void> => {
  logger.recManager.info('create task queue (start)', { id: rec.id });

  try {
    await getQueue().createRecordTask({
      ...rec,
      action: TaskActionType.Create,
    });
    logger.recManager.info('create task queue (start)', { id: rec.id });
  } catch (err) {
    throw { rec, step: FinalStep.task };
  }
};

export const init = (config: TRecordingConfig) => {
  const journal = new Journal<TFinalActionError>();

  const createJournalTask = {
    [FinalStep.dump]: (rec: TFinalDescDump): void => {
      journal.createTask(
        {
          rec,
          step: FinalStep.dump,
        },
        config.timeoutOnError.saveDumpToCloud
      );
    },
    [FinalStep.task]: (rec: TFinalDescTask): void => {
      journal.createTask(
        {
          rec,
          step: FinalStep.task,
        },
        config.timeoutOnError.createProcessingTask
      );
    },
  };

  const finalStepError = (err: TFinalActionError): TFinalActionError => {
    logger.recJournal.error(ELogErrorCode.RecJournalExecution, 'error', {
      step: err.step,
      id: err.rec.id,
    });

    createJournalTask[err.step](err.rec);

    return err;
  };

  const executeJournalTask = {
    [FinalStep.dump]: async (rec: TFinalDescDump): Promise<void | TFinalActionError> => {
      logger.recJournal.info('task-run', {
        step: FinalStep.dump,
        id: rec.id,
      });

      try {
        const src = await saveDumpToCloud(rec);

        await createTaskProcess({ ...rec, src });
      } catch (err) {
        finalStepError(err);
      }
    },
    [FinalStep.task]: async (rec: TFinalDescTask): Promise<void | TFinalActionError> => {
      logger.recJournal.info('task-run', {
        step: FinalStep.task,
        id: rec.id,
      });

      try {
        await createTaskProcess(rec);
      } catch (err) {
        finalStepError(err);
      }
    },
  };

  journal.onTask((task) => void executeJournalTask[task.step](task.rec));

  const removeJournalTask = journal;

  return { journal, removeJournalTask, executeJournalTask, createJournalTask };
};
