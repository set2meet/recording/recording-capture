import { ServerOptions } from 'https';

type GError = {
  code: number;
  message: string;
};

// config

export type TRecordingConfig = {
  httpsOptions?: ServerOptions;
  workingDir: string;
  maxConnections: number;
  timeoutOnError: {
    saveDumpToCloud: number; // ms
    createProcessingTask: number; // ms
  };
};

// init record

export type TPayloadOfferInit = {
  recordId?: string;
  type?: string; // media container type (webm, mp4)
};

// record media: start

export type TPayloadOfferStart = {
  recordId: string;
  offer: RTCSessionDescription;
};
