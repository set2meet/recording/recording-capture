if(!/(darwin|linux)/.test(process.platform)) {
  throw new Error('Unsupported platform, must be darwin or linux.');
}
