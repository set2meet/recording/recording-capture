const need_version = 10;
const cur_version = parseInt(process.versions.node);

if (cur_version !== need_version) {
  throw new Error(`Need node = ${need_version}`);
}
