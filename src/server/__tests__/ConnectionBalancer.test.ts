/* eslint-disable max-statements */
import ConnectionBalancer, { MAX_CONNECTIONS } from '../ConnectionBalancer';

const MAX_FOR_TEST = 3;

describe('balancer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  test('blocked', () => {
    const balancer = new ConnectionBalancer(MAX_FOR_TEST);

    expect(balancer.blocked).toBeFalsy();

    balancer.add();
    expect(balancer.blocked).toBeFalsy();

    balancer.add();
    expect(balancer.blocked).toBeFalsy();

    balancer.add();
    expect(balancer.blocked).toBeTruthy();

    balancer.del();
    expect(balancer.blocked).toBeFalsy();

    balancer.add();
    expect(balancer.blocked).toBeTruthy();

    balancer.del();
    expect(balancer.blocked).toBeFalsy();

    balancer.del();
    expect(balancer.blocked).toBeFalsy();
  });

  test('blocked with default params', () => {
    const balancer = new ConnectionBalancer();

    for (let i = 0; i < MAX_CONNECTIONS; i++) {
      expect(balancer.blocked).toBeFalsy();
      balancer.add();
    }

    expect(balancer.blocked).toBeTruthy();
  });
});
