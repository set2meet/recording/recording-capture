import logger from '../logger/logger';

type TDelta = -1 | 1;

export const MAX_CONNECTIONS = 150;

export default class ConnectionBalancer {
  constructor(
    private maxConnections: number = MAX_CONNECTIONS,
    private blockedFlag: boolean = false,
    private counter: number = 0
  ) {}

  public add(): void {
    this.update(1);
  }

  public del(): void {
    this.update(-1);
  }

  public get blocked(): boolean {
    return this.blockedFlag;
  }

  private update(delta: TDelta): void {
    const action = delta === 1 ? 'connected' : 'disconnected';

    this.blockedFlag = (this.counter += delta) === this.maxConnections;

    logger.balancer.info('update', {
      action,
      counter: this.counter,
      blocked: this.blockedFlag,
    });
  }
}
