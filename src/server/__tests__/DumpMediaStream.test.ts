/* eslint-disable max-statements, @typescript-eslint/unbound-method, @typescript-eslint/no-unsafe-member-access, 
@typescript-eslint/no-explicit-any  */
jest.mock('semantic-sdp');
jest.mock('medooze-media-server');

import { mock } from 'jest-mock-extended';
import { SDPInfo, StreamInfo } from 'semantic-sdp';
import MediaServer, {
  Recorder,
  Transport,
  IncomingStream,
  OutgoingStream,
  IncomingStreamTrack,
  Endpoint,
} from 'medooze-media-server';

import DumpMediaStream, { TDumpMediaStreamOptions } from '../DumpMediaStream';

describe('DumpMediaStream', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  test('works properly', () => {
    const fakeAnswerSDP = 'fake answer sdp';
    const offer = mock<SDPInfo>();
    const answer = mock<SDPInfo>();
    const recorder = mock<Recorder>();
    const endpoint = mock<Endpoint>();
    const transport = mock<Transport>();
    const streamInfo = mock<StreamInfo>();
    const inStream = mock<IncomingStream>();
    const inAudioTrack = mock<IncomingStreamTrack>();
    const inVideoTrack = mock<IncomingStreamTrack>();
    const outStream = mock<OutgoingStream>();
    const outStreamInfo = mock<StreamInfo>();

    (MediaServer as any).recorder = recorder;

    (SDPInfo.parse as jest.Mock).mockReturnValue(offer);

    (offer.answer as jest.Mock).mockReturnValue(answer);
    (offer.getStreams as jest.Mock).mockReturnValue([streamInfo]);

    // (answer.toString as jest.Mock).mockReturnValue(fakeAnswerSDP); // it doesn't work
    Object.defineProperty(answer, 'toString', { value: () => fakeAnswerSDP });

    (endpoint.createTransport as jest.Mock).mockReturnValue(transport);

    (transport.createIncomingStream as jest.Mock).mockReturnValue(inStream);
    (transport.createOutgoingStream as jest.Mock).mockReturnValue(outStream);
    (transport.on as jest.Mock).mockImplementation((event, listener: (track: IncomingStreamTrack) => void) =>
      listener(inAudioTrack)
    );

    (outStream.getStreamInfo as jest.Mock).mockReturnValue(outStreamInfo);

    (inStream.getAudioTracks as jest.Mock).mockReturnValue([inAudioTrack]);
    (inStream.getVideoTracks as jest.Mock).mockReturnValue([inVideoTrack]);

    (inAudioTrack.on as jest.Mock).mockImplementation((event, listener: () => void) => listener());

    const description: RTCSessionDescription = {
      sdp: 'fake offer sdp',
      type: null,
      toJSON: null,
    };

    const options: TDumpMediaStreamOptions = {
      output: 'out',
      description,
      endpoint,
    };
    const dms = new DumpMediaStream(options);

    expect(transport.setRemoteProperties).toHaveBeenCalled();
    expect(transport.setLocalProperties).toHaveBeenCalled();
    expect(outStream.attachTo).toHaveBeenCalledWith(inStream);
    expect(answer.addStream).toHaveBeenCalledWith(outStreamInfo);
    expect(recorder.record).toHaveBeenCalledWith(inStream);
    expect(recorder.stop).not.toHaveBeenCalled();
    expect(transport.stop).not.toHaveBeenCalled();

    const answerStruct = dms.answer;

    expect(answerStruct.type).toEqual('answer');
    expect(answerStruct.sdp).toEqual(fakeAnswerSDP);

    dms.stop();

    expect(recorder.stop).toHaveBeenCalled();
    expect(transport.stop).toHaveBeenCalled();

    dms.stop(); // repeated method invocation should not lead to problems
  });

  test('no description', () => {
    const options: TDumpMediaStreamOptions = {
      output: 'out',
      description: null,
      endpoint: null,
    };

    expect(() => new DumpMediaStream(options)).toThrow(Error);
  });
});
