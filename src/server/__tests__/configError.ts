jest.mock('env-config');
jest.mock('fs-extra');

import * as cfg from 'env-config';
import { buildConfig } from '../config';
import * as fs from 'fs-extra';

type TTestConfig = {
  [key: string]: string | number | TTestConfig;
};

const initTestConfig = (data: TTestConfig): void => {
  (cfg.readConfig as jest.Mock).mockImplementation((): TTestConfig => data);
};

const disableDirAccess = (): void => {
  (fs.ensureDirSync as jest.Mock).mockImplementation((): void => {
    throw new Error('forbidden');
  });
};

describe('config', () => {
  beforeEach(() => {
    jest.spyOn(process, 'exit').mockImplementation(() => {
      throw new Error('stop');
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  test('empty', async () => {
    initTestConfig({});

    expect(await buildConfig()).toBeDefined();
  });

  test('no path', async () => {
    initTestConfig({
      recordingCapture: {
        workingDir: null,
      },
    });

    expect(await buildConfig()).toBeDefined();
  });

  test('bad path', async () => {
    disableDirAccess();
    initTestConfig({
      recordingCapture: {
        workingDir: '/bad_folder',
      },
    });

    try {
      await buildConfig();
      expect(true).toBeFalsy();
    } catch (e) {
      expect(e).toBeDefined();
    }
  });
});
