import path from 'path';
import fs from 'fs-extra';
import { TRecordingConfig } from '../types/app';
import logger from '../logger/logger';
import { ELogErrorCode } from '../logger/ELogErrorCode';
import { readConfig, IRecordingCaptureConfig, ICaptureConfig } from 'env-config';

const BASE_DIR = '../../';

const showErrorAndStop = (err: Error) => {
  logger.config.error(ELogErrorCode.Config, err.message);
  process.exit(1);
};

const hasWorkingDirAccess = (url: string): void => {
  try {
    fs.ensureDirSync(url);
    fs.accessSync(url, fs.constants.W_OK);
  } catch (err) {
    throw new Error(`no acces to directory ${url}`);
  }
};

const validateNumericValue = (value: number | string, defaultValue: number): number => {
  if (typeof value !== 'number') {
    return parseInt(value, 10) || defaultValue;
  }

  return value;
};

export const buildConfig = async (): Promise<TRecordingConfig> => {
  const config = await readConfig<IRecordingCaptureConfig>();
  const captureConfig = config.recordingCapture || ({} as ICaptureConfig);
  const resultConfig: TRecordingConfig = {
    workingDir: 'tmp',
    maxConnections: 150,
    timeoutOnError: {
      saveDumpToCloud: 3600000, // 1 hour
      createProcessingTask: 3600000, // 1 hour
    },
  };

  if (config.httpsOptions) resultConfig.httpsOptions = config.httpsOptions;

  // find correct path of working directory
  if (captureConfig.workingDir) {
    const workingDir: string = captureConfig.workingDir;

    if (workingDir.indexOf('/') === 0 || workingDir.indexOf('~/') === 0) {
      resultConfig.workingDir = workingDir;
    } else {
      resultConfig.workingDir = path.resolve(__dirname, BASE_DIR, workingDir);
    }
  } else {
    resultConfig.workingDir = path.resolve(__dirname, BASE_DIR, resultConfig.workingDir);
  }

  try {
    // check working dir access
    hasWorkingDirAccess(resultConfig.workingDir);

    // ensure some external props
    if (captureConfig) {
      const { maxConnections, timeoutOnError } = captureConfig;
      const { maxConnections: defaultMaxConnections, timeoutOnError: defaultTimeoutOnError } = resultConfig;

      resultConfig.maxConnections = validateNumericValue(maxConnections, defaultMaxConnections);
      resultConfig.timeoutOnError.saveDumpToCloud = validateNumericValue(
        timeoutOnError?.saveDumpToCloud,
        defaultTimeoutOnError.saveDumpToCloud
      );
      resultConfig.timeoutOnError.createProcessingTask = validateNumericValue(
        timeoutOnError?.createProcessingTask,
        defaultTimeoutOnError.createProcessingTask
      );
    }

    return resultConfig;
  } catch (e) {
    showErrorAndStop(e);
  }
};

export default buildConfig;
