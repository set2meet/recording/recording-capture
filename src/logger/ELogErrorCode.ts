export enum ELogErrorCode {
  Application = 'E_APPLICATION',
  Config = 'E_CONFIG',
  RecManagerExecution = 'E_RECMANAGER_EXECUTION',
  RecJournalExecution = 'E_JOURNAL_EXECUTION',
}
