import path from 'path';
import uuid from 'uuid';
import { TPayloadOfferInit, TPayloadOfferStart, TRecordingConfig } from '../types/app';
import { checkDirExistanceOrCreate, getNextRecordFileName } from './utils';
import logger from '../logger/logger';
import DumpMediaStream from './DumpMediaStream';
import { init as initJournal, TFinalActionError, TFinalDescDump, TFinalDescTask } from './journal/journal';
import { FinalStep } from './journal/FinalStep';
import { Endpoint } from 'medooze-media-server';
import Journal from '@s2m/journal';

enum recManagerStatus {
  ready = 'ready',
  initInProcess = 'init:in-process',
  initCompleted = 'init:completed',
  recording = 'recording',
  finalInProgress = 'final:in-progress',
  stopped = 'stopped',
}

export default class RecordingCaptureService {
  private config: TRecordingConfig;
  private socket: SocketIO.Socket;
  private recordId: string;
  private type: string;
  private outputFilePath: string;
  private workingDirectory: string;
  private dumpStream: DumpMediaStream;
  private status: recManagerStatus = recManagerStatus.ready;
  private connectionId: string = undefined;
  private endpoint: Endpoint;
  private journal: Journal<TFinalActionError>;
  private executeJournalTask: Record<
    FinalStep,
    (rec: TFinalDescDump | TFinalDescTask) => Promise<void | TFinalActionError>
  >;
  private createJournalTask: Record<FinalStep, (rec: TFinalDescDump | TFinalDescTask) => void>;
  private logger = logger.recManager;

  constructor(config: TRecordingConfig, socket: SocketIO.Socket, endpoint: Endpoint) {
    this.config = config;
    this.socket = socket;
    this.connectionId = socket.id;
    this.endpoint = endpoint;

    const { journal, executeJournalTask, createJournalTask } = initJournal(config);

    this.journal = journal;
    this.executeJournalTask = executeJournalTask;
    this.createJournalTask = createJournalTask;

    this.log('created');

    this.socket.on('record-init-offer', this.init);
    this.socket.on('record-start-offer', this.start);
    this.socket.on('record-stop-offer', this.finish);
    this.socket.on('record-suspend-offer', this.suspend);
    this.socket.once('disconnect', this.socketDisconnect);
  }

  //eslint-disable-next-line
  private log(message: string, data?: any): void {
    this.logger?.info({ ...data, connectionId: this.connectionId });
  }

  //eslint-disable-next-line max-statements
  private init = (payload: TPayloadOfferInit): void => {
    const status = recManagerStatus.ready;

    if (this.status !== status) {
      return this.initErr(this.wrongStatusError(status));
    }

    // if this is a continuation of the recording,
    //  need to delete the task from the journal
    if (payload.recordId) {
      this.journal.removeTask({
        rec: { id: this.recordId },
      });
    }

    const { workingDir } = this.config;

    this.log('init offer', { payload });

    this.status = recManagerStatus.initInProcess;
    this.recordId = payload.recordId || uuid();
    this.type = payload.type;

    this.workingDirectory = path.resolve(workingDir, this.recordId);

    this.log('init', {
      recordId: this.recordId,
      workingDir: this.workingDirectory,
    });

    try {
      checkDirExistanceOrCreate(this.workingDirectory);
      this.initAnswer(getNextRecordFileName(this.workingDirectory));
    } catch (err) {
      this.initErr(err);
    }
  };

  private initAnswer(name: string): void {
    // build full path top output file
    this.outputFilePath = path.resolve(this.workingDirectory, name);
    this.log('init answer', { output: this.outputFilePath });

    // send answer: service ready to dumb media
    this.socket.emit('record-init-answer', {
      recordId: this.recordId,
    });

    // update status
    this.status = recManagerStatus.initCompleted;
  }

  private initErr(err: Error): void {
    this.log('init error', {
      error: err.message,
    });
    this.status = recManagerStatus.ready;
    this.socket.emit('record-init-answer', { error: err });
  }

  private start = (payload: TPayloadOfferStart): void => {
    const status = recManagerStatus.initCompleted;

    if (this.status !== status) {
      return this.startErr(this.wrongStatusError(status));
    }

    try {
      this.dumpStream = new DumpMediaStream({
        output: this.outputFilePath,
        description: payload.offer, //eslint-disable-line
        endpoint: this.endpoint,
      });
    } catch (err) {
      return this.startErr(err);
    }

    this.socket.emit('record-start-answer', {
      recordId: this.recordId,
      answer: this.dumpStream.answer,
    });

    this.status = recManagerStatus.recording;
  };

  private startErr(err: Error): void {
    this.log('start error', {
      error: err.message,
    });
    this.status = recManagerStatus.initCompleted;
    this.socket.emit('record-init-answer', { error: err });
  }

  private stop = () => {
    if (this.dumpStream) {
      this.dumpStream.stop();
    }
  };

  private suspend = (): void => {
    this.log('suspend');

    this.stop();

    if (this.socket) {
      this.socket.emit('record-suspend-answer', {
        recordId: this.recordId,
      });
    }
  };

  private finish = (): void => {
    this.log('stop offer');

    this.stop();

    if (this.socket) {
      this.socket.emit('record-stop-answer', {
        recordId: this.recordId,
      });
    }

    // finalize dumped record

    this.status = recManagerStatus.finalInProgress;

    void this.executeJournalTask[FinalStep.dump]({
      id: this.recordId,
      src: this.workingDirectory,
      type: this.type,
    }).then((err: TFinalActionError) => {
      if (!err) {
        this.resetModel();
      }
    });
  };

  private resetModel(): void {
    this.recordId = null;
    this.log('finished success');
    this.status = recManagerStatus.stopped;
  }

  private socketDisconnect = (): void => {
    this.stop();

    const endStatus = [recManagerStatus.finalInProgress, recManagerStatus.stopped];

    this.log('disconnect', {
      status: this.status,
    });
    this.socket = null;
    this.connectionId = null;

    // create final step actions
    if (endStatus.indexOf(this.status) === -1) {
      this.log('create-journal-task', {
        after: 'disconnect',
        status: this.status,
      });
      this.createJournalTask[FinalStep.dump]({
        id: this.recordId,
        src: this.workingDirectory,
        type: this.type,
      });
    }
  };

  private wrongStatusError(status: recManagerStatus): Error {
    return new Error(`wrong status "${this.status}", must be ${status}`);
  }
}
