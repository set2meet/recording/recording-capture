﻿/* eslint-disable max-statements, @typescript-eslint/unbound-method, @typescript-eslint/no-unsafe-member-access,
@typescript-eslint/no-explicit-any  */

import Journal from '@s2m/journal';
import { mock } from 'jest-mock-extended';
import { TFinalActionError, TFinalDescDump, TFinalDescTask } from '../journal/journal';
import { FinalStep } from '../journal/FinalStep';

jest.spyOn(process, 'exit').mockImplementation(() => {
  throw new Error('--stop--');
});

jest.mock('@s2m/logger');
jest.mock('socket.io');
jest.mock('semantic-sdp');
jest.mock('fs-extra');
jest.mock('@s2m/journal');
jest.mock('@s2m/journal/lib/dump', () => ({
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  read: () => {},
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  write: () => {},
}));
jest.mock('@s2m/cloud');
jest.mock('../DumpMediaStream');
jest.mock('../journal/journal', () => {
  const executeJournalTask = {
    [FinalStep.dump]: () => Promise.resolve(),
  };

  return {
    init: () => ({
      journal: mock<Journal<TFinalActionError>>(),
      createJournalTask: mock<Record<FinalStep, (rec: TFinalDescDump | TFinalDescTask) => void>>(),
      executeJournalTask,
    }),
  };
});

import SocketIO from 'socket.io';
import { Endpoint } from 'medooze-media-server';
import * as fs from 'fs-extra';
import { TPayloadOfferInit, TPayloadOfferStart, TRecordingConfig } from '../../types/app';
import DumpMediaStream from '../DumpMediaStream';
import RecordingCaptureService from '../RecordingCaptureService';

interface IEvent {
  event: string;
  args: any[];
}

const config: TRecordingConfig = {
  workingDir: 'workingDir',
  maxConnections: 10,
  timeoutOnError: {
    saveDumpToCloud: 100,
    createProcessingTask: 100,
  },
};

describe('RecordingCaptureService', () => {
  const offer: RTCSessionDescription = {
    sdp: 'fake offer sdp',
    type: null,
    toJSON: null,
  };

  const endpoint = mock<Endpoint>();
  const socket = mock<SocketIO.Socket>();
  let initListener: (payload: TPayloadOfferInit) => void;
  let startListener: (payload: TPayloadOfferStart) => void;
  let stopListener: () => void;
  let suspendListener: () => void;
  let disconnectListener: () => void;
  let events: IEvent[] = [];
  let dump: DumpMediaStream = null;

  const getNextEvent = (): IEvent => {
    expect(events.length).toBeGreaterThan(0);

    return events.shift();
  };

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    initListener = null;
    startListener = null;
    stopListener = null;
    suspendListener = null;
    disconnectListener = null;
    events = [];
    dump = null;

    (DumpMediaStream as jest.Mock).mockImplementation(() => {
      dump = mock<DumpMediaStream>();

      (dump as any).answer = 'some answer';

      return dump;
    });

    (socket.on as jest.Mock).mockImplementation((event: string, listener: () => void) => {
      switch (event) {
        case 'record-init-offer':
          initListener = listener as (payload: TPayloadOfferInit) => void;
          break;

        case 'record-start-offer':
          startListener = listener as (payload: TPayloadOfferStart) => void;
          break;

        case 'record-stop-offer':
          stopListener = listener;
          break;

        case 'record-suspend-offer':
          suspendListener = listener;
          break;
      }
    });

    (socket.once as jest.Mock).mockImplementation((event: string, listener: () => void) => {
      switch (event) {
        case 'disconnect':
          disconnectListener = listener;
          break;
      }
    });

    (socket.emit as jest.Mock).mockImplementation((event: string, ...args: any[]) => {
      const data: IEvent = {
        event,
        args: args || [],
      };

      events.push(data);
    });

    (fs.readdirSync as jest.Mock).mockReturnValue(['part-0.mp4', 'part-1.mp4', 'part-2.mp4']);

    new RecordingCaptureService(config, socket, endpoint);

    expect(initListener).toBeDefined();
    expect(startListener).toBeDefined();
    expect(stopListener).toBeDefined();
    expect(suspendListener).toBeDefined();
    expect(disconnectListener).toBeDefined();
  });

  afterAll(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  test('works properly', () => {
    // init

    initListener({
      type: 'mp4',
    });

    const initEvent = getNextEvent();

    expect(initEvent.event).toEqual('record-init-answer');
    expect(initEvent.args[0].recordId).toBeDefined();

    const recordId = initEvent.args[0].recordId as string;

    // start

    startListener({
      recordId,
      offer,
    });

    const startEvent = getNextEvent();

    expect(startEvent.event).toEqual('record-start-answer');
    expect(startEvent.args[0].answer).toEqual('some answer');
    expect(dump).not.toBeNull();

    // suspend

    suspendListener();

    const suspendEvent = getNextEvent();

    expect(suspendEvent.event).toEqual('record-suspend-answer');
    expect(suspendEvent.args[0].recordId).toEqual(recordId);

    // stop

    stopListener();

    const stopEvent = getNextEvent();

    expect(stopEvent.event).toEqual('record-stop-answer');
    expect(dump.stop).toBeCalled();

    // disconnect

    disconnectListener();
  });

  test('resume', () => {
    const recordId = 'some record id';

    // init with recordId

    initListener({
      type: 'mp4',
      recordId,
    });

    const initEvent = getNextEvent();

    expect(initEvent.event).toEqual('record-init-answer');
    expect(initEvent.args[0].recordId).toEqual(recordId);

    // start

    startListener({
      recordId,
      offer,
    });

    const startEvent = getNextEvent();

    expect(startEvent.event).toEqual('record-start-answer');
    expect(startEvent.args[0].answer).toEqual('some answer');
    expect(dump).not.toBeNull();

    // stop

    stopListener();

    const stopEvent = getNextEvent();

    expect(stopEvent.event).toEqual('record-stop-answer');
    expect(dump.stop).toBeCalled();

    // disconnect

    disconnectListener();
  });

  test('disconnect suddenly', () => {
    initListener({
      type: 'mp4',
    });

    getNextEvent();

    disconnectListener();
  });

  test('init wrong status', () => {
    initListener({
      type: 'mp4',
    });

    getNextEvent();

    initListener({
      type: 'mp4',
    });

    const event = getNextEvent();

    expect(event.event).toEqual('record-init-answer');

    expect(event.args[0].error).toBeDefined();
  });

  test('start wrong status', () => {
    initListener({
      type: 'mp4',
    });

    getNextEvent();

    startListener({
      recordId: 'some record',
      offer,
    });

    getNextEvent();

    startListener({
      recordId: 'some record',
      offer,
    });

    const event = getNextEvent();

    expect(event.event).toEqual('record-init-answer');

    expect(event.args[0].error).toBeDefined();
  });
});
