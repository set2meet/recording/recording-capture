import path from 'path';
import fs from 'fs-extra';
import logger from '../logger/logger';

const deleteDirWithNoFiles = (dir: string): void => {
  const items = fs.readdirSync(dir);

  if (items.length === 0) {
    fs.rmdirSync(dir);
    logger.recFs.info('Remove dir as empty', { dir });
  }
};

const recordPattern = new RegExp(/part-\d+\.mp4/);

const filterRecords = (files: string[]): string[] => {
  return files.filter((file) => recordPattern.exec(file));
};

const numberPattern = new RegExp(/\d+/);

const getNextMaxNumber = (files: string[]): number => {
  return (
    files.reduce((num, file) => {
      return Math.max(num, parseInt(numberPattern.exec(file)[0], 10));
    }, 0) + 1
  );
};

const getRecordFileName = (num: number): string => `part-${num}.mp4`;

export const checkDirExistanceOrCreate = (dir: string): void => {
  fs.ensureDirSync(dir);
};

export const getNextRecordFileName = (dir: string): string => {
  const maxNum = getNextMaxNumber(filterRecords(fs.readdirSync(dir)));

  return getRecordFileName(maxNum);
};

export const clearEmptyDirectories = (dir: string): void => {
  const names = fs.readdirSync(dir);

  names.forEach((name) => {
    deleteDirWithNoFiles(path.resolve(dir, name));
  });
};
