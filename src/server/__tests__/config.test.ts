process.env.NODE_ENV = 'test';

jest.mock('fs-extra');

import { buildConfig } from '../config';

const MAX_CONNECTION_TEST_VALUE = 5;
const DUMP_TEST_VALUE = 3600001;
const TASK_TEST_VALUE = 3600002;

describe('config', () => {
  test('works properly', async () => {
    const config = await buildConfig();

    expect(config).toBeDefined();
    expect(config.maxConnections).toBe(MAX_CONNECTION_TEST_VALUE);
    expect(config.timeoutOnError.saveDumpToCloud).toBe(DUMP_TEST_VALUE);
    expect(config.timeoutOnError.createProcessingTask).toBe(TASK_TEST_VALUE);
  });
});
