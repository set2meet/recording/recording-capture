# Recording capture service

Dumps stream (audio/video) to mp4 files, and upload to s3 cloud all dumped files (part-N.mp4)

## Installation

1. see virtual-room readme

## Development

Run local recording-capture service:

- `npm run server:dev:run` - just run the server with out watching changes in local dependencies
- `npm run server:dev` - run the server with watching changes in local dependencies

### Config

Config has next structure:

```json
{
  "recordingProcessing": {
    "workingDir": "./tmp",
    "maxConnections": 2
  }
}
```

```
| field name     |  type  | required | default | description                                                                                                                                |
| -------------- | :----: | :------: | ------: | ------------------------------------------------------------------------------------------------------------------------------------------ |
| workingDir     | string |    -     |     tmp | the path to the working directory where draft files will be downloaded and other manipulations are performed to obtain the finished result |
| maxConnections | number |    -     |     150 | maximum number of simultaneous connections to the service                                                                                  |
```
### Container delivery

To create docker container container, do the next steps

1. [Installation](#installation): steps 1-4
2. create config for `recording-capture`
   1. clone `env-config` into the `project` folder: `git clone git@gitlab.com:set2meet/env-config.git`
   2. build config, run next command inside `env-config` folder `npm run build -- --module recording-capture --stand production`
   3. copy folder `env-config/config` to `./config` (to the `project` folder)
3. Build docker image in context of `project` folder by the command like `docker build -t recording-capture:test -f recording-capture/Dockerfile .`

## Build with Multistage Docker

#### Requirements:

- Install Docker version 18.0+
- In order to use ssh-key mapping inside docker you should enable buildkit and experemental features using your daemon.json:

```json
{
  "experimental": true,
  "features": {
    "buildkit": true
  }
}
```

##### Build

If you have config file in git folder, docker build script will use it in docker image. Otherwise docker try to generete env-config using `npx`. Please be careful.

`docker build -t recording-capture --ssh default="~/.ssh/id_rsa" --build-arg STAND=local --file=Multistage.Dockerfile .`

Default is path to your ssh key for gitlab.com. For Windows user it should be `%USERPROFILE%\.ssh\id_rsa`

Use `--build-arg STAND=local` to change config to new-production/local/development

### Docker build params

For running this service through Docker you should provide options for env-config:
`-module {module-name} -stand {stand-name} -path {output-config-path} -url {hashicorp-url} -namespace {hashicorp-namespace} -role_id {CI-role-id} -secret_id {CI-secret-id} -npmScripts {script1, script2,...}`
where:
- `{module-name}`: name of the service (module) for which we want to build the config, without brackets
- `{stand-name}`: name of the stand for which we want config will be use, without brackets, or use `local` for local development
- `{output-config-path}`: path for your config
- `{CI-role-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
- `{CI-secret-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
- `{hashicorp-url}`: For custom hashicorp url.
- `{hashicorp-namespace}`: For custom hashicorp namespace.
- `{script1, script2,...}`: Array with _NPM_ scripts that will be executed after merging static and secret parts in memory. Inside code, it transforms into the: `concurrently "npm run script1" "npm run script2"... `


##### Run

Base command: `docker run -d recording-capture --name recording-capture --port=[wsPort] --endpoints-port-range=[minPort-MaxPort] --behind-load-balancer=[true|false] [ip]`

where

1. `wsPort` - ws port (default: 3350)
2. `minPort-maxPort` is UDP port range for endpoints (default: 50000-60000)
3. `ip` - External IP address of server, to be used when announcing the local ICE candidate (default: 127.0.0.1)
4. `behind-load-balancer` - if application is running behind load balancer, then it uses unsecured connections for communications between services, because load balancer response for security (default false)

Also don't forget to publish ports.

Make attention:

1. [minPort-MaxPort] - server outs configured ports (it must be the same as your minPort-maxPort params): `RTPTransport::SetPortRange() | configured RTP/RTCP ports range`
   1. if port rang is too small, then meduza server will not use this param, so set at least 50 ports,
   2. minPort-maxPort must be even numbers
2. [ip] - must be your public/external ip, 127.0.0.1 didn't work on my local computer.
   1. browser/client must see server by this ip
3. the vast majority of problems associated with incorrect settings of [minPort-MaxPort] and [ip], so browser cannot find how to send vidio stream to recording-capture,
   for detecting network problems you can open the page `chrome://webrtc-internals/` in chrome and check `icegatheringstatechange` messages (value must be `complete/connected`).

Example:
`docker run --rm -p "60000-60100:60000-60100/udp" -p "3350:3350" recording-capture --endpoints-port-range=60000-60100 192.168.56.1`,
